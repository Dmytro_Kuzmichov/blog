<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container"> 
    <h2> Feedback</h2>

    <table class="table">
     <thead class="thead-dark">
        <tr>
         <th> Name </th>
         <th> Email </th>
         <th> Phonenumber </th>
         <th> Message </th>
         <th> Image </th>
        </tr>
     </thead>
    <tbody>
        @foreach ($feedback as $feed)
            <tr>
                <td>{{ $feed->name }}</td>
                <td>{{ $feed->email }}</td>
                <td>{{ $feed->phonenumber }}</td>
                <td>{{ $feed->msg }}</td>
                <td>
                    @isset ($feed->image)
                        <img class="img-fluid" src="{{asset('uploads/feedback/' . $feed->image) }}" width="320">
                    @endisset 
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</body>
</html>
