<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1> Send mail</h1>

    <p>Имя: {{ $name }}</p>
    <p>Email: {{ $email }}</p>
    <p>Телефон: {{ $phonenumber }}</p>
    <p>Сообщение: {{ $msg }}</p>
    <p>Изображение: </p>
    @isset ($image)
        <img class="img-fluid" src="{{asset('uploads/feedback/' . $image) }}">
    @endisset

</body>
</html>
