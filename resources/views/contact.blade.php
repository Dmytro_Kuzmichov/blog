@extends('layouts.app')
  
@section('content')
     <div class="container">
        <h1 class="mb-2 text-left">Форма обратной связи</h1>
        
        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status')}}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-12 col-md-6 ">
            <div class="row justify-content-md-center">
                <form class="form-horizontal" method="post" action="/contact" enctype="multipart/form-data">
                    @csrf  
                    <div class="form-group">
                        <label for="name">Имя: </label>
                        <input type="text" class="form-control" id="name" placeholder="Ваше имя" name="name" required>
                    </div>
    
                    <div class="form-group">
                        <label for="email">Email: </label>
                        <input type="text" class="form-control" id="email" placeholder="john@example.com" name="email" required>
                    </div>

                    <div class="form-group">
                        <label for="phone">Телефон: </label>
                        <input type="text" class="form-control" id="phonenumber" placeholder="+380-00-000-00-00" name="phonenumber" >
                    </div>

                    <div class="form-group">
                    <input type="file" name="image" multiple>
                        <label for="file-input">Выберите файл</label>
                    </div>

                    <div class="form-group">
                        <label for="message">Сообщение: </label>
                        <textarea type="text" class="form-control luna-message" id="msg" placeholder="Текст письма" name="msg" required></textarea>
                    </div>
    
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" value="Send">Отправить</button>
                    </div>
                </form>
                @isset ($image)
                    <img class="img-fluid" src="{{asset('uploads/feedback/' . $feedback->image) }}">
                @endisset
            </div>
        </div>
    </div>
@endsection