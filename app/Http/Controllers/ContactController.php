<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Feedback as Back;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Feedback;
use Illuminate\Foundation\Validation\Validation;
use Illuminate\Support\Facades\DB;


class ContactController extends Controller
{
    public function index()
    {
        return view('contact');
    }
    
    public function send(Request $request)
    {   
        if($request->isMethod('post')) {
            $rules =  [
                'name' => 'required|alpha|max:10',
                'email' => 'required|email',
                'msg' => 'required|max:255',
                'phonenumber' => 'digits_between:10,12',
                'image' => 'required|image|mimes:jpeg,png,jpg,bmp,|max:2048',
                
                ];
            $this->validate($request,$rules);
        }
            
        $name = $request->name;
        $email = $request->email;
        $msg = $request->msg;
        $phonenumber = $request->phonenumber;
        $image = $request->image;

        //create email in temp-mail.org and enter here
        
        $result = Mail::to('bawedam308@imailt.com')
        ->send(new Feedback($name, $email, $msg, $phonenumber, $image));
        
        $feedback = new Back();
        $feedback->name = $request->input('name');
        $feedback->email = $request->input('email');
        $feedback->msg = $request->input('msg');
        $feedback->phonenumber = $request->input('phonenumber');
        $feedback->image = $request->file('image');


            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('uploads/feedback/', $filename);
                $feedback->image = $filename;
            } else {
                return $request;
                $feedback->image = '';
            }

            if($feedback) {
                $feedback->save();
                return redirect()->back()->with('status', 'Email is send');
            }

    }

    public function show()
    {
        $feedback = DB::table('feedback')->get();
        return view('feedback.feedback', ['feedback' => $feedback ]);
    }

}


        