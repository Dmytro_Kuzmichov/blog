<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;

class AccountController extends Controller
{
    public function show()
    {
        $feedback = DB::table('feedback')->get();
        return view('feedback.feedback', ['feedback' => $feedback ]);
    }

    public function getUsers()
    {
        $users = User::all();
        return view('admin.users', compact('users'));
    }
}
