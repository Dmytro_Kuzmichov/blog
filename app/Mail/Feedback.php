<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class Feedback extends Mailable
{
    use Queueable, SerializesModels;

    //protected $data;
    protected $name;
    protected $email;
    protected $msg;
    protected $phonenumber;
    protected $image;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $msg, $phonenumber, $image)
    {
        $this->name = $name;
        $this->email = $email;
        $this->msg = $msg;
        $this->phonenumber = $phonenumber;
        $this->image = $image;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.feedback')
        ->with([
            'name' => $this->name,
            'email' => $this->email,
            'msg' => $this->msg,
            'phonenumber' => $this->phonenumber,
            'image' => $this->image,
        ])
        ->subject('Новое письмо');
    }
}
