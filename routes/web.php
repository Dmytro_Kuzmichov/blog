<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::group(['middleware' => 'auth'], function() {
    Route::get('/feedback',  'ContactController@show');
});
Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/contact',  'ContactController@send'); 


Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function() {
    
    Route::get('/users', 'Admin\AccountController@getUsers'); 
});
